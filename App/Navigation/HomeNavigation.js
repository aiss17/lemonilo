import React from 'react'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import { Image, Text, View } from 'react-native'
import Images from '../Assets/Images'

//Icon
import AntDesign from "react-native-vector-icons/AntDesign";

//Pages
import Home from '../Pages/HomePages/Home'
import Products from '../Pages/HomePages/Products'

export default TabNavigator = createBottomTabNavigator(  
    {  
        Home: { 
            screen: Home,  
            navigationOptions:{  
                header: null,
                tabBarLabel: ({ tintColor }) => (
                    <Text style={[styles.label, { color: tintColor }]}>Home</Text>
                ),  
                tabBarIcon: ({ tintColor }) => (  
                    <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 5}}>  
                        <Image source={Images.home} style={{width: 25, height: 25, tintColor}}/>
                        {/* <AntDesign name="home" style={{width: 25, height: 25, tintColor}} /> */}
                    </View>
                ),  
            }  
        },  
        MyActivity: { 
            screen: Products,  
            navigationOptions:{  
                header: null,
                tabBarLabel: ({ tintColor }) => (
                    <Text style={[styles.label, { color: tintColor }]}>Activity</Text>
                ), 
                tabBarIcon: ({ tintColor }) => (  
                    <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 5}}>  
                        <Image source={Images.list} style={{width: 25, height: 25, tintColor}}/>
                    </View>
                ),
            }  
        },
        Payment: { 
            screen: Products,  
            navigationOptions:{  
                header: null,
                tabBarLabel: ({ tintColor }) => (
                    <Text style={[styles.label, { color: tintColor }]}>Payment</Text>
                ), 
                tabBarIcon: ({ tintColor }) => (  
                    <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 5}}>  
                        <Image source={Images.wallet} style={{width: 25, height: 25, tintColor}}/>
                    </View>
                ),
            }  
        },
        Messages: { 
            screen: Products,  
            navigationOptions:{  
                header: null,
                tabBarLabel: ({ tintColor }) => (
                    <Text style={[styles.label, { color: tintColor }]}>Messages</Text>
                ), 
                tabBarIcon: ({ tintColor }) => (  
                    <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 5}}>  
                        <Image source={Images.message} style={{width: 25, height: 25, tintColor}}/>
                    </View>
                ),
            }  
        },
        Account: { 
            screen: Products,  
            navigationOptions:{  
                header: null,
                tabBarLabel: ({ tintColor }) => (
                    <Text style={[styles.label, { color: tintColor }]}>Account</Text>
                ), 
                tabBarIcon: ({ tintColor }) => (  
                    <View style={{ justifyContent: 'center', alignSelf: 'center', paddingTop: 5}}>  
                        <Image source={Images.account} style={{width: 25, height: 25, tintColor}}/>
                    </View>
                ),
            }  
        }
    },  
    {  
        tabBarOptions: {
            inactiveTintColor: 'gray',
            activeTintColor: '#00b14f',
            style: {
                backgroundColor: '#FFFFFF',
                width: '100%',
            },
            labelStyle: {
                color: 'black'
            }
        },
    },  
);  

const styles = {
    label: {
        textAlign: 'center', fontSize: 12, marginBottom: 5
    }
}
const Images = {
    splashScreen: require('./grab.jpg'),
    home: require('./iconHome.png'),
    list: require('./iconList.png'),
    account: require('./iconProfile.png'),
    message: require('./iconNotif.png'),
    wallet: require('./wallet.png'),
    walletPlus: require('./walletPlus.png'),
    ovo: require('./ovo.png'),
}

export default Images;
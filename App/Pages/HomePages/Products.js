import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, Alert, StyleSheet, FlatList, Image, TextInput } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import axios from 'axios'
import Images from '../../Assets/Images';
import Icon from 'react-native-vector-icons/AntDesign'
import Modal from 'react-native-modal';

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataProducts: [],
            backUpListProduct: [],
            visibleEdit: false,
            visibleAdd: false,
            id: '',
            editName: '',
            editDetail: ''
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    // onDidFocus={() => this.getDataProducts()}
                />

                <Text style={{fontSize: 30}}>SOON</Text>
            </View>
        )
    }
}

export default Products;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems:'center'
    },
})
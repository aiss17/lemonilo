import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, StyleSheet, Alert, Image, Dimensions, ScrollView, FlatList, ImageBackground } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import {Searchbar} from 'react-native-paper'
import AntDesign from 'react-native-vector-icons/AntDesign'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import Images from '../../Assets/Images'

let windowWidth = Dimensions.get('window').width;
let windowHeight = Dimensions.get('window').height;

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            search: '',
            data: [
                {
                    nama: "Test",
                    diskon: "Diskon s.d 50rb",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Promo",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Diskon s.d 50rb",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Diskon 40rb",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Diskon s.d 50rb",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Gratis",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Diskon s.d 50rb",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "Gratis",
                    tgl: 'Until 28 Feb'
                },
                {
                    nama: "Test",
                    diskon: "",
                    tgl: 'Until 28 Feb'
                }
            ]
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    onScroll = (event) => {
        const { navigation } = this.props;
        const currentOffset = event.nativeEvent.contentOffset.y;
        const dif = currentOffset - (this.offset || 0);  
    
        if (dif < 0) {
            alert('scroll up')
        } else {
            alert('scroll down')
        }
        //console.log('dif=',dif);
    
        this.offset = currentOffset;
    } 

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    // onDidFocus={() => this.checkDataLogin()}
                />

                <ScrollView 
                    onScroll={(e) => this.onScroll(e)}                
                >
                    <View style={styles.ViewHeader}>
                        <Searchbar
                            placeholder="Offers, food, and places to go"
                            onChangeText={(text) => this.setState({ search: text }, () => alert(this.state.search))}
                            value={this.state.search}
                            style={styles.searchBar}
                            inputStyle={{ textAlign: 'center', fontSize: 13}}
                            icon={() => <AntDesign name='scan1' size={25} />}
                            onIconPress={() => alert('Triggered for scan QR')}
                        />
                    </View>

                    <View style={styles.viewOvo}>
                        <TouchableOpacity
                            style={styles.btnOvoLeft}
                        >
                            <View style={styles.circle}>
                                <Image source={Images.ovo} style={{height: 7, width: 23}} />
                            </View>
                            <Text style={styles.textRp}>
                                Rp
                            </Text>
                            <Text>
                                2500
                            </Text>
                            <MaterialIcons name='keyboard-arrow-right' size={20} color='#999999' />
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={styles.btnOvoRight}
                        >
                            <View style={styles.circle}>
                                <MaterialCommunityIcons name='crown' size={18} color='#30cc70' />
                            </View>
                            <Text>
                                Hello, OVO Poin...
                            </Text>
                            <MaterialIcons name='keyboard-arrow-right' size={20} color='#999999' />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.viewWallet}>
                        <TouchableOpacity
                            style={styles.btnWallet}
                        >
                            <Image source={Images.walletPlus} style={{width: 20, height: 20, marginRight: 10, tintColor: '#30cc70'}} />
                            <Text style={styles.textTopUp}>Top Up</Text>
                            <Text style={styles.textTitik}>.</Text>
                            <Text>Wallet</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ height: 1, backgroundColor: '#baccbf', marginBottom: 5, marginTop: 15 }} />
                    
                    <Text  style={{ fontSize: 20, marginLeft: 15, fontWeight: 'bold' }}>Buatmu si Petualang Rasa</Text>
                    <FlatList 
                        data={this.state.data}
                        numColumns={2}
                        renderItem={({ item, index }) => {
                            return(
                                <TouchableOpacity
                                    style={{ 
                                        flex: 1,
                                        margin: 15,
                                        borderRadius: 20
                                    }}
                                >
                                    <ImageBackground
                                        source={Images.splashScreen}
                                        style={{ height: 150,
                                            justifyContent: 'flex-start',
                                            alignItems: 'flex-start'
                                        }}
                                    >
                                        {item.diskon != "" && (
                                            <Text style={{ color: '#fff', fontWeight: 'bold', textAlign: 'center', backgroundColor: 'black', padding: 5, marginTop: 5, marginLeft: 5 }} >
                                                {item.diskon}
                                            </Text>
                                        )}
                                    </ImageBackground>
                                    <Text style={{fontSize: 15}}>{item.nama}</Text>
                                    <View style={{flexDirection: 'row', marginTop: 5}}>
                                        <MaterialCommunityIcons name='calendar-blank' size={15} color='#999999' />
                                        <Text style={{fontSize: 12, color: '#999999', marginLeft: 5}}>{item.tgl}</Text>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </ScrollView>
            </View>
        )
    }
}

export default Home;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    ViewHeader : {
        height: 80,
        backgroundColor: '#30cc70',
        justifyContent: 'center',
        alignItems: 'center'
    },
    searchBar: {
        height: 40,
        width: 350,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnOvoLeft: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 4,
        borderRightWidth: 0.5,
        borderBottomWidth: 1,
        borderColor: '#d4d4d4'
    },
    btnOvoRight: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 4,
        borderLeftWidth: 0.5,
        borderBottomWidth: 1,
        borderColor: '#d4d4d4'
    },
    viewOvo: {
        flexDirection: 'row',
        height: 60
    },
    circle: {
        width: 32,
        height: 32,
        borderRadius: 32/2,
        marginRight: 10,
        borderWidth: 2,
        borderColor: '#d4d4d4',
        justifyContent: 'center',
        alignItems: 'center'
        // backgroundColor: 'red'
    },
    textRp: {
        fontSize: 10,
        marginRight: 5,
        color: '#999999',
        marginBottom: 5
    },
    viewWallet: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    btnWallet: {
        marginTop: 15,
        justifyContent:'center',
        alignItems: 'center',
        flexDirection: 'row',
        height: 50,
        width: 180,
        borderTopWidth: 0.5,
        borderRightWidth: 0.5,
        borderLeftWidth: 0.5,
        borderBottomWidth: 1,
        borderColor: '#999999',
        borderRadius: 5
    },
    textTopUp: {
        fontWeight: 'bold',
        marginRight: 3
    },
    textTitik: {
        fontWeight: 'bold',
        marginBottom: 20,
        fontSize: 25,
        marginRight: 3
    }
})